package password;
/*
 * @author Nupur Adhvaryu 991518688
 * Assume spaces are not valid characters for the purpose of calculating length
 */
public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_NUM = 2;
	
	public static boolean checkPwd(String pwd) {
		return(pwd !=null && !pwd.contains(" ") && pwd.length() >= MIN_LENGTH);
		
		
	}
	
	public static boolean numCheck(String pwd) {
		boolean valid = true;
		int count = 0;
		
		for(int i = 0 ; i<pwd.length(); i++) {
			if(Character.isDigit(pwd.charAt(i))) {
			count ++;
			}
		}
		if(count >=2) {
			return valid;
		}
		else
			return false;
		
	}
	
}
