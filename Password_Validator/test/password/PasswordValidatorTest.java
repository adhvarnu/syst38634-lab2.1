package password;
//fail("invalid length of password");

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Nupur Adhvaryu 991518688
 */
public class PasswordValidatorTest {

	
	@Test
	public void testnumcheckRegular(){
//		boolean pwdnum = PasswordValidator.numCheck("45123");
//		assertTrue("invalid password entered", pwdnum);
		//fail();
	}
	
	@Test
	public void testnumcheckException(){
//		boolean pwdnum = PasswordValidator.numCheck("1");
//		assertFalse("invalid password entered", pwdnum);
		fail();
	}
	@Test
	public void testcheckPwdspaceBoundaryOut() {
//		boolean pwdlength = PasswordValidator.checkPwd("        ");
//		assertFalse("invalid password entered", pwdlength);
		//fail();
	}
	@Test
	public void testnumcheckBoundaryOut(){
//		boolean pwdnum = PasswordValidator.numCheck("1helo");
//		assertFalse("invalid password entered", pwdnum);
		//fail();
	}
		
	@Test
	public void testnumcheckBoundaryIn(){
//		boolean pwdnum = PasswordValidator.numCheck("12hello");
//		assertTrue("invalid password entered", pwdnum);
		//fail();	
		}
	
	@Test
	public void testcheckPwdspaceException() {
//		boolean pwdlength = PasswordValidator.checkPwd("Nupur nice");
//		assertFalse("invalid password entered", pwdlength);
		//fail();
	}

	
	@Test 
	public void testcheckPwdExceptionNull() {
		boolean pwdlength = PasswordValidator.checkPwd(null);
		assertFalse("invalid password entered", pwdlength);
	}
	
	@Test
	public void testcheckPwdRegular() {
		boolean pwdlength = PasswordValidator.checkPwd("helloNupur");
		assertTrue("invalid password entered", pwdlength);
	}
	
	@Test 
	public void testcheckPwdException() {
		boolean pwdlength = PasswordValidator.checkPwd("Nupur");
		assertFalse("invalid password entered", pwdlength);
	}
	
	@Test
	public void testcheckPwdBoundaryOut() {
		boolean pwdlength = PasswordValidator.checkPwd("Nupur12");
		assertFalse("invalid password entered", pwdlength);
	}
	
	@Test
	public void testcheckPwdBoundaryIn() {
		boolean pwdlength = PasswordValidator.checkPwd("Nupur123");
		assertTrue("invalid password entered", pwdlength);
	}
}
